from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {"todo_list_list": lists, "page": "lists"}
    return render(request, "todos/list.html", context)


@login_required
def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {"todo_list": todo_list, "page": "detail"}
    return render(request, "todos/detail.html", context)


@login_required
def todo_list_create(request):
    if request.method == "POST":
        todo_form = TodoForm(request.POST)
        if todo_form.is_valid():
            list = todo_form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        todo_form = TodoForm()
    context = {
        "form": todo_form,
        "title": "Create a new list",
        "page": "create list",
    }
    return render(request, "todos/create.html", context)


@login_required
def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_form = TodoForm(request.POST, instance=todo_list)
        if todo_form.is_valid():
            todo_form.save()
            return redirect("todo_list_detail", id=id)
    else:
        todo_form = TodoForm(instance=todo_list)
    context = {
        "todo_list": todo_list,
        "form": todo_form,
        "title": "Update List",
        "page": "edit list",
    }
    return render(request, "todos/edit.html", context)


@login_required
def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    context = {"list_name": todo_list.name, "page": "delete list"}
    return render(request, "todos/delete.html", context)


@login_required
def todo_item_create(request):
    if request.method == "POST":
        item_form = ItemForm(request.POST)
        if item_form.is_valid():
            item = item_form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        item_form = ItemForm()
    context = {
        "form": item_form,
        "title": "Create a new item",
        "page": "create item",
    }
    return render(request, "todos/create.html", context)


@login_required
def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        item_form = ItemForm(request.POST, instance=todo_item)
        if item_form.is_valid():
            item_form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        item_form = ItemForm(instance=todo_item)
    context = {
        "todo_item": todo_item,
        "form": item_form,
        "title": "Update Item",
        "page": "edit item",
    }
    return render(request, "todos/edit.html", context)
